# WebPack Project template

This here is a simple project template for getting started on smaller web projects that require only js and css.

The main aim is that it build all assets as needed as well as lints through code, throwing problems as warnings to make development easier.

## Javascript
- Edit at `src/assets/js`, files are bundled into `web/js/bundle.js`
- Babel is included in configuration, but can be removed if not needed.
- Also linting is included with the eslint-standard rules.

## SCSS
- Edit at `src/assets/scss`, files are build under `web/css/base.css`. The aim is that you can use these files to create base styles for the project. If you wish to add other files instead of one, you have to remember to include that in the `web/index.html` as well. Its expected you'd import the styles in your js-components for use etc...

```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 ```
