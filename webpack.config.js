const path = require('path');
const glob = require('glob');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: [
    ...glob.sync(path.resolve(__dirname, 'src/assets/js/*.js')),
    ...glob.sync(path.resolve(__dirname, 'src/assets/scss/*.scss')),
  ],
  output: {
    path: path.resolve(__dirname, 'web'),
    filename: 'js/bundle.js',
    publicPath: '/',
  },
  module: {
    rules: [
      // Remove the babel-loader below if not needed
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
      // Lint javascript through eslint
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          emitWarning: true,
        },
      },
      // Build scss-files to css-files
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'css/[name].css',
            },
          },
          {
            loader: 'extract-loader',
          },
          {
            loader: 'postcss-loader',
          },
          {
            loader: 'sass-loader',
          },
        ],
      },
    ],
  },
  plugins: [
    new StyleLintPlugin({
      configFile: '.stylelintrc.json',
      context: path.resolve(__dirname, 'src/assets/scss'),
      files: ['**/*.scss'],
      failOnError: false,
      emitErrors: false,
      syntax: 'scss',
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/public/index.html'),
      inject: 'body',
    }),
  ],
  devServer: {
    contentBase: './src/public',
    port: 8888,
  },
};
